FROM alpine:3.7
LABEL Yulia Kriulina <0937630@hr.nl>

RUN apk update
RUN apk add nodejs 
RUN apk add nginx
RUN set -x ; \
  addgroup -g 82 -S www-data ; \
  adduser -u 82 -D -S -G www-data www-data && exit 0 ; exit 1

COPY ./nginx.conf	    /etc/nginx/nginx.conf
COPY ./frontend /frontend
WORKDIR frontend
RUN npm config set unsafe-perm true
RUN npm install
RUN npm install -g @angular/cli
RUN ng config -g cli.warnings.versionMismatch false
RUN ng build --prod

#COPY ./frontend/dist/frontend   /www
